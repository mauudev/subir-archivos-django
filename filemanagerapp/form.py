from django import forms
from filemanagerapp.models import Upload

class UploadForm(forms.ModelForm):
    class Meta:
        model = Upload
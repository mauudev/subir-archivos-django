from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'myfilesapp.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #PASO 2: AGREGAR LOS IMPORT Y LA URL DE LA APP Y AL FINAL LA LINEA DE CODIGO QUE SIGUE
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'filemanagerapp.views.home', name='imageupload'),
    )+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
